﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Open.Nat;

public class UdpNetworkClient : UdpNetwork {

    string sidneysHostAddress = "83.86.27.64";
    string tyronesHostAddress = "94.197.121.27";
    string loopBackAddress = "127.0.0.1";

    Player player;

    void Awake() {
        udpClient = new UdpClient();
        remoteIpEndPoint = new IPEndPoint(IPAddress.Parse(sidneysHostAddress), 7500);
        udpClient.Connect(remoteIpEndPoint);
        StartNetworkThread();

        player = GameObject.Find("Player").GetComponent<Player>();
    }

    void Update() {

        if (Input.GetKeyDown(KeyCode.UpArrow)) {
            Enter("UpArrow");
            Debug.Log("Up pressed");
        }

        if (Input.GetKeyDown(KeyCode.DownArrow)) {
            Enter("DownArrow");
            Debug.Log("Down pressed");
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow)) {
            Enter("LeftArrow");
            Debug.Log("Left pressed");
        }

        if (Input.GetKeyDown(KeyCode.RightArrow)) {
            Enter("RightArrow");
            Debug.Log("Right pressed");
        }
    }

    public void Enter(string key) {

        Byte[] senddata = Encoding.ASCII.GetBytes(key);
        udpClient.Send(senddata, senddata.Length);
    }

    protected override void CustomNetworkThreadAsync() {
        string returnData = ReceiveData();

        if (returnData.Equals("UpArrow")) {
            player.stats.z += 1;
        }

        if (returnData.Equals("DownArrow")) {
            player.stats.z -= 1;
        }

        if (returnData.Equals("LeftArrow")) {
            player.stats.x += -1;
        }

        if (returnData.Equals("RightArrow")) {
            player.stats.x += 1;
        }
    }
}
