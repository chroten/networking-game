﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Open.Nat;

public class UdpNetworkServer : UdpNetwork {
    Player player;

    void Awake() {
        udpClient = new UdpClient(7500);
        remoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
        ManagePortforwardingAsync(StartNetworkThread);

        player = GameObject.Find("Player").GetComponent<Player>();
    }

    protected override void CustomNetworkThreadAsync() {

        string returnData = ReceiveData();
        Debug.Log(remoteIpEndPoint.Address.ToString() + ": " + returnData.ToString());

        if (returnData.Equals("UpArrow")) {
            player.stats.z += 1;
        }

        if (returnData.Equals("DownArrow")) {
            player.stats.z += -1;
        }

        if (returnData.Equals("LeftArrow")) {
            player.stats.x += -1;
        }

        if (returnData.Equals("RightArrow")) {
            player.stats.x += 1;
        }

        SendData(returnData);
    }

    #region Portforwarding 
    void ManagePortforwardingAsync(Action callback) {
        var nat = new NatDiscoverer();
        var cts = new CancellationTokenSource();
        cts.CancelAfter(5000);

        NatDevice device = null;

        var t = nat.DiscoverDeviceAsync(PortMapper.Upnp, cts);

        t.ContinueWith(tt => {
            device = tt.Result;
            device.GetExternalIPAsync()
                .ContinueWith(task => {
                    return device.CreatePortMapAsync(new Mapping(Protocol.Udp, 7500, 7500, "UDP Server Console"));
                });

        }, TaskContinuationOptions.OnlyOnRanToCompletion);

        try {
            t.Wait();
            if (t.IsCompleted) {
                callback();
            }
        } catch (AggregateException e) {
            if (e.InnerException is NatDeviceNotFoundException) {
                Debug.Log("Port forwarding failed: No device found");
            }
        }
    }
    #endregion
}
