﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Open.Nat;

public abstract class UdpNetwork : MonoBehaviour {

    protected UdpClient udpClient; // need more specific name
    protected IPEndPoint remoteIpEndPoint;
    protected Thread networkThread;

    protected void StartNetworkThread() {
        networkThread = new Thread(new ThreadStart(NetworkThreadAsync));
        networkThread.Start();
    }

    void OnApplicationQuit() {
        udpClient.Close();
        networkThread.Abort();
    }

    protected string ReceiveData() {
        Byte[] receiveBytes = udpClient.Receive(ref remoteIpEndPoint);
        string returnData = Encoding.ASCII.GetString(receiveBytes);
        return returnData;
    }

    protected void SendData(string data) {
        Byte[] senddata = Encoding.ASCII.GetBytes(data);
        udpClient.Send(senddata, senddata.Length, remoteIpEndPoint);
    }

    protected void NetworkThreadAsync() {
        while (true) {
            CustomNetworkThreadAsync();
        }
    }

    protected abstract void CustomNetworkThreadAsync();
}
