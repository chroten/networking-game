﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    internal PlayerStats stats;

    // Use this for initialization
    void Start () {
        stats = new PlayerStats();
    }
	
	// Update is called once per frame
	void Update () {
        Debug.Log(stats.x);
        transform.position = new Vector3(stats.x, stats.y, stats.z);
	}
}
