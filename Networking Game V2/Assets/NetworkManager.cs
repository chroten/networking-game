﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManager : MonoBehaviour {


    public GameObject GameClientPrefab;
    public GameObject GameServerPrefab;


    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void CreateServer() {

        Instantiate(GameServerPrefab);

    }

    public void CreateClient() {

        Instantiate(GameClientPrefab);

    }
}
