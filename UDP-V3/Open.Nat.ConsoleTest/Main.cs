//
// Authors:
//   Ben Motmans <ben.motmans@gmail.com>
//
// Copyright (C) 2007 Ben Motmans
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Open.Nat.ConsoleTest {
    class UDPServer {

        public static void Main(string[] args) {
            ManagePortforwardingAsync(StartUDPServer);
            Console.ReadKey();
        }

        public static void StartUDPServer() {
            Thread thdUDPServer = new Thread(new ThreadStart(ServerThreadAsync));
            thdUDPServer.Start();
        }

        public static void ServerThreadAsync() {

            UdpClient udpClient = new UdpClient(7500);
            while (true) {
                IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
                Byte[] receiveBytes = udpClient.Receive(ref RemoteIpEndPoint);
                string returnData = Encoding.ASCII.GetString(receiveBytes);
                Console.WriteLine(RemoteIpEndPoint.Address.ToString() + ": " + returnData.ToString());

            }
        }

        static void ManagePortforwardingAsync(Action callback) {
            var nat = new NatDiscoverer();
            var cts = new CancellationTokenSource();
            cts.CancelAfter(5000);

            NatDevice device = null;

            var t = nat.DiscoverDeviceAsync(PortMapper.Upnp, cts);

            t.ContinueWith(tt => {
                device = tt.Result;
                device.GetExternalIPAsync()
                    .ContinueWith(task => {
                        return device.CreatePortMapAsync(new Mapping(Protocol.Udp, 7500, 7500, "UDP Server Console"));
                    });

            }, TaskContinuationOptions.OnlyOnRanToCompletion);

            try {
                t.Wait();
                if (t.IsCompleted) {
                    callback();
                }
            } catch (AggregateException e) {
                if (e.InnerException is NatDeviceNotFoundException) {
                    Console.WriteLine("Not found");
                    Console.WriteLine("Press any key to exit...");
                }
            }
        }
    }
}