﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace UDP_Client_Console {
    class UDPClientConsole {


        static UdpClient udpClient = new UdpClient();

        static void Main(string[] args) {

            string sidneysHostAddress = "83.86.27.64";
            string tyronesHostAddress = "94.197.121.27";
            string loopBackAddress = "127.0.0.1";

            udpClient.Connect(sidneysHostAddress, 7500);

            ConsoleKeyInfo cki;
            do {
                cki = Console.ReadKey();
                Enter(cki.Key.ToString());
                Console.WriteLine(cki.Key.ToString());
            } while (cki.Key != ConsoleKey.Escape);
        }

        static void Enter(string key) {
            
            Byte[] senddata = Encoding.ASCII.GetBytes(key);
            udpClient.Send(senddata, senddata.Length);
        }
    }
}
